<?php

namespace Drupal\webform_zendesk\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zendesk\API\HttpClient as ZendeskAPI;

/**
 * Form submission to MailChimp handler.
 *
 * @WebformHandler(
 *   id = "zendesk",
 *   label = @Translation("Zendesk"),
 *   category = @Translation("Zendesk"),
 *   description = @Translation("Sends a form submission to Zendesk to create a support ticket."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformZendeskHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTranslationManagerInterface
   */
  protected $token_manager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, WebformTokenManagerInterface $token_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->tokenManager = $token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('webform.token_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#theme' => 'markup',
      '#markup' => '<strong>' . $this->t('Subdomain') . ': </strong>' . $this->configuration['subdomain'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'subdomain' => '',
      'email' => '',
      'name' => '',
      'subject' => '',
      'message' => '',
      'additional_data' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['zendesk'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Zendesk settings'),
    ];

    $form['zendesk']['subdomain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zendesk Subdomain'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['subdomain'],
    ];

    $fields = $this->getWebform()->getElementsDecoded();
    $options_email = $options_text = $options_textarea = ['' => $this->t('- Select an option -')];
    foreach ($fields as $field_name => $field) {
      if ($field['#type'] == 'email') {
        $options_email[$field_name] = $field['#title'];
      }
      if ($field['#type'] == 'textfield') {
        $options_text[$field_name] = $field['#title'];
      }
      if ($field['#type'] == 'textarea') {
        $options_textarea[$field_name] = $field['#title'];
      }
    }
    $form['zendesk']['email'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Requester Email field'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['email'],
      '#options' => $options_email,
    ];

    $form['zendesk']['subject'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Subject'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['subject'],
      '#options' => $options_text,
    ];

    $form['zendesk']['message'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['message'],
      '#options' => $options_textarea,
    ];

    $form['zendesk']['additional_data'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Additional data'),
      '#default_value' => $this->configuration['additional_data'],
      '#description' => $this->t('You can use this field to set addional data to add to the <a href=":zendesk_uri">request</a>. You may use tokens.', [':zendesk_uri' => 'https://developer.zendesk.com/rest_api/docs/core/requests#create-request']),
    ];

    $form['zendesk']['token_tree_link'] = $this->tokenManager->buildTreeLink();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values['zendesk'][$name])) {
        $this->configuration[$name] = $values['zendesk'][$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // If update, do nothing
    if ($update) {
      return;
    }

    $fields = $webform_submission->toArray(TRUE);

    // Replace tokens.
    $configuration = $this->tokenManager->replace($this->configuration, $webform_submission);

    // Allow for either values coming from other fields or static/tokens
    foreach (['email', 'subject', 'message'] as $field) {
      $$field = $configuration[$field];
      if (!empty($fields['data'][$configuration[$field]])) {
        $$field = $fields['data'][$configuration[$field]];
      }
    }

    $zendesk = [
      'requester' => [
        'email' => $email,
      ],
      'subject' => $subject,
      'comment' => ['body' => $message]
    ];

    $additional_data = Yaml::decode($configuration['additional_data']);
    $zendesk = array_replace_recursive($zendesk, $additional_data);

    $client = new ZendeskAPI($configuration['subdomain']);

    try {
      $ticket = $client->requests()->create($zendesk);
    }
    catch (\Exception $exception) {
      $message = $exception->getMessage();

      // Encode HTML entities to prevent broken markup from breaking the page.
      $message = nl2br(htmlentities($message));

      // Log error message.
      $context = [
        '@exception' => get_class($request_exception),
        '@form' => $this->getWebform()->label(),
        '@state' => $state,
        '@message' => $message,
        'link' => $this->getWebform()->toLink($this->t('Edit'), 'handlers')->toString(),
      ];
      $this->getLogger()->error('@form webform submission to zendesk failed. @exception: @message', $context);
    }
  }
}
